"""Main script for generating output.csv."""
"""Test done by Nikhita Agarwal"""
import pandas

def AVGfunc(HorPValue,SideValue,SubjectValue):
 # add basic program logic here
	data =pandas.read_csv('data/raw/pitchdata.csv')
	
	#AVG calculation	
	#HorPValue = Hitter or Pitcher , SideValue = 'L' or 'R'
	df = pandas.DataFrame(data,columns = [SubjectValue,HorPValue,'H','AB','PA'])
	df.columns.values[0] = 'SubjectId'
	if (HorPValue == 'HitterSide'):
	    df1 = df[df.HitterSide==SideValue].groupby('SubjectId',sort=True).agg({'H':'sum','AB':'sum','PA':'sum'})
	else:
	    df1 = df[df.PitcherSide==SideValue].groupby('SubjectId',sort=True).agg({'H':'sum','AB':'sum','PA':'sum'})
	df1["Value"] = df1.loc[:,"H"].div(df1["AB"],axis=0)
	df1["Stat"] = "AVG"
	df1["Subject"] = SubjectValue
	if (HorPValue == 'HitterSide'):
	    if (SideValue=='R'):
	        df1["Split"] = "vs RHH"
	    else:
	        df1["Split"] = "vs LHH"
	else:
	    if (SideValue=='R'):
	        df1["Split"] = "vs RHP"
	    else:
	        df1["Split"] = "vs LHP"
	df1_having = df1[df1.PA >= 25].round(3)
	
	df1_final = pandas.DataFrame(df1_having,columns = ['Stat','Split','Subject','Value'])
	return(df1_final)

def OBPfunc(HorPValue,SideValue,SubjectValue):
 # add basic program logic here
	data =pandas.read_csv('data/raw/pitchdata.csv')
	
	#OBP calculation
	df = pandas.DataFrame(data,columns = [SubjectValue,HorPValue,'H','BB','HBP','AB','SF','PA'])
	df.columns.values[0] = 'SubjectId'
	if (HorPValue == 'HitterSide'):
	    df2 = df[df.HitterSide==SideValue].groupby('SubjectId',sort=True,).sum()
	else:
	    df2 = df[df.PitcherSide==SideValue].groupby('SubjectId',sort=True).sum()
	df_numerator = pandas.DataFrame(df2,columns = ['H','BB','HBP'])
	df_denominator = pandas.DataFrame(df2,columns = ['AB','BB','SF','HBP'])
	df_numerator["Value"] = df_numerator.sum(axis=1)
	df_denominator["Value"] = df_denominator.sum(axis=1)
	df2["Value"] = df_numerator.loc[:,"Value"].div(df_denominator["Value"],axis=0)
	df2["Stat"] = "OBP"
	df2["Subject"] = SubjectValue
	if (HorPValue == 'HitterSide'):                  
	    if (SideValue=='R'):
	        df2["Split"] = "vs RHH"
	    else:
	        df2["Split"] = "vs LHH"
	else:
	    if (SideValue=='R'):
	        df2["Split"] = "vs RHP"
	    else:
	        df2["Split"] = "vs LHP"
	df2_having = df2[df2.PA >= 25].round(3)
	
	df2_final = pandas.DataFrame(df2_having,columns = ['Stat','Split','Subject','Value'])
	return(df2_final)
	
def SLGfunc(HorPValue,SideValue,SubjectValue):
 # add basic program logic here
	data =pandas.read_csv('data/raw/pitchdata.csv')
	
	#SLG calculation	
	df = pandas.DataFrame(data,columns = [SubjectValue,HorPValue,'TB','AB','PA'])
	df.columns.values[0] = 'SubjectId'
	if (HorPValue == 'HitterSide'):
	    df1 = df[df.HitterSide==SideValue].groupby('SubjectId',sort=True).agg({'TB':'sum','AB':'sum','PA':'sum'})
	else:
	    df1 = df[df.PitcherSide==SideValue].groupby('SubjectId',sort=True).agg({'TB':'sum','AB':'sum','PA':'sum'})
	df1["Value"] = df1.loc[:,"TB"].div(df1["AB"],axis=0)
	df1["Stat"] = "SLG"
	df1["Subject"] = SubjectValue
	if (HorPValue == 'HitterSide'):                  
	    if (SideValue=='R'):
	        df1["Split"] = "vs RHH"
	    else:
	        df1["Split"] = "vs LHH"
	else:
	    if (SideValue=='R'):
	        df1["Split"] = "vs RHP"
	    else:
	        df1["Split"] = "vs LHP"
	df1_having = df1[df1.PA >= 25].round(3)
	
	df1_final = pandas.DataFrame(df1_having,columns = ['Stat','Split','Subject','Value'])
	return(df1_final)

def OPSfunc(HorPValue,SideValue,SubjectValue):
    #OPS = OBP + SLG #taking the values from the two functions
    data =pandas.read_csv('data/raw/pitchdata.csv')
    df_p1 = OBPfunc(HorPValue,SideValue,SubjectValue)
    df_part1 = pandas.DataFrame(df_p1,columns = ['Value'])
    df_p2 = SLGfunc(HorPValue,SideValue,SubjectValue)
    df_part2 = pandas.DataFrame(df_p2,columns = ['Value'])
    df = pandas.DataFrame(data,columns = [SubjectValue,HorPValue,'PA'])
    df.columns.values[0] = 'SubjectId'
    if (HorPValue == 'HitterSide'):
       df1 = df[df.HitterSide==SideValue].groupby('SubjectId',sort=True).agg({'PA':'sum'})
    else:
	   df1 = df[df.PitcherSide==SideValue].groupby('SubjectId',sort=True).agg({'PA':'sum'})
    df1["Value"] = df_part1.add(df_part2,fill_value=0)
    df1["Stat"] = "OPS"
    df1["Subject"] = SubjectValue
    if (HorPValue == 'HitterSide'):                  
	    if (SideValue=='R'):
	        df1["Split"] = "vs RHH"
	    else:
	        df1["Split"] = "vs LHH"
    else:
	    if (SideValue=='R'):
	        df1["Split"] = "vs RHP"
	    else:
	        df1["Split"] = "vs LHP"
    df1_having = df1[df1.PA >= 25].round(3)
	
    df1_final = pandas.DataFrame(df1_having,columns = ['Stat','Split','Subject','Value'])
  
    return(df1_final)
	
	
def main():
     #combinations 
     df1 = AVGfunc('PitcherSide','R','HitterId')
     df2 = OBPfunc('PitcherSide','R','HitterId')
     df3 = SLGfunc('PitcherSide','R','HitterId')
     df4 = OPSfunc('PitcherSide','R','HitterId')
     df5 = AVGfunc('PitcherSide','L','HitterId')
     df6 = OBPfunc('PitcherSide','L','HitterId')
     df7 = SLGfunc('PitcherSide','L','HitterId')
     df8 = OPSfunc('PitcherSide','L','HitterId')
     df9 = AVGfunc('PitcherSide','R','HitterTeamId')
     df10 = OBPfunc('PitcherSide','R','HitterTeamId')
     df11 = SLGfunc('PitcherSide','R','HitterTeamId')
     df12 = OPSfunc('PitcherSide','R','HitterTeamId')
     df13 = AVGfunc('PitcherSide','L','HitterTeamId')
     df14 = OBPfunc('PitcherSide','L','HitterTeamId')
     df15 = SLGfunc('PitcherSide','L','HitterTeamId')
     df16 = OPSfunc('PitcherSide','L','HitterTeamId') 
     df17 = AVGfunc('HitterSide','R','PitcherId')
     df18 = OBPfunc('HitterSide','R','PitcherId')
     df19 = SLGfunc('HitterSide','R','PitcherId')
     df20 = OPSfunc('HitterSide','R','PitcherId')
     df21 = AVGfunc('HitterSide','L','PitcherId')
     df22 = OBPfunc('HitterSide','L','PitcherId')
     df23 = SLGfunc('HitterSide','L','PitcherId')
     df24 = OPSfunc('HitterSide','L','PitcherId')
     df25 = AVGfunc('HitterSide','R','PitcherTeamId')
     df26 = OBPfunc('HitterSide','R','PitcherTeamId')
     df27 = SLGfunc('HitterSide','R','PitcherTeamId')
     df28 = OPSfunc('HitterSide','R','PitcherTeamId')
     df29 = AVGfunc('HitterSide','L','PitcherTeamId')
     df30 = OBPfunc('HitterSide','L','PitcherTeamId')
     df31 = SLGfunc('HitterSide','L','PitcherTeamId')
     df32 = OPSfunc('HitterSide','L','PitcherTeamId')
    
	 #final data frame which is the union of all the combinations
     df_final = pandas.concat([df1, df2, df3, df4, df5, df6, df7, df8, df9, df10, df11, df12, df13, df14, df15,
	 df16, df17, df18, df19, df20, df21, df22, df23, df24, df25, df26, df27, df28, df29, df30, df31, df32])
    
     #sorting by 3 columns 
     df_sorted = df_final.sort_values(['Stat','Split','Subject'], ascending=[True,True,True])
     df_sorted.to_csv('data/processed/output.csv', sep=',') 
     pass
    


if __name__ == '__main__':
    main()
